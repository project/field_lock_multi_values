<?php

declare(strict_types=1);

namespace Drupal\Tests\field_lock_multi_values\Functional;

use PHPUnit\Framework\Attributes\Group;

/**
 * Test "Lock previous value" feature.
 */
#[Group('field_lock_multi_values')]
class LockPreviousValueTest extends TestBase {

  /**
   * Test "Lock previous value" feature.
   */
  public function testLockPreviousValue() : void {
    $page = $this->getSession()->getPage();
    $delta = 4;
    // Submit a test node.
    $this->createTestNode($delta);
    // Test node edit form with original functionality.
    $this->drupalGet('/node/1/edit');
    for ($i = 0; $i <= $delta; $i++) {
      $this->assertSession()
        ->fieldEnabled("edit-field-string-textfield-$i-value");
    }
    // enable "Lock Previous Value" feature.
    $this->drupalGet('/admin/structure/types/manage/page/form-display');
    $this->click('#edit-fields-field-string-textfield-settings-edit');
    $page->checkField('Lock previous value');
    $this->submitForm([], 'Update');
    $this->submitForm([], 'Save');
    // Test node edit form with updated functionality.
    $this->drupalGet('/node/1/edit');
    for ($i = 0; $i <= $delta; $i++) {
      $this->assertSession()
        ->fieldDisabled("edit-field-string-textfield-$i-value");
    }
  }
}
