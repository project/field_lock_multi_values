<?php

declare(strict_types=1);

namespace Drupal\Tests\field_lock_multi_values\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;
use PHPUnit\Framework\Attributes\Group;

/**
 * Generic test base class.
 */
#[Group('field_lock_multi_values')]
abstract class TestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field_lock_multi_values',
    'node'
  ];

  /**
   * @var \Drupal\node\Entity\NodeType $nodeType
   */
  protected NodeType $nodeType;

  /**
   * @var \Drupal\user\Entity\User $user
   */
  protected User $user;

  /**
   * @var string $field_id
   *
   * Tested field identifier.
   */
  protected string $field_id = 'field_string_textfield';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->nodeType = $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'Basic page',
    ]);
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'node',
      'field_name' => $this->field_id,
      'type' => 'string',
      'cardinality' => -1,
    ]);
    $field_storage->save();
    $field = [
      'field_name' => $this->field_id,
      'label' => 'String textfield',
      'entity_type' => 'node',
      'bundle' => 'page',
      'required' => TRUE,
    ];
    FieldConfig::create($field)->save();
    // Assign form settings.
    \Drupal::service('entity_display.repository')->getFormDisplay('node', 'page')
      ->setComponent($this->field_id)
      ->save();
    // Assign display settings.
    \Drupal::service('entity_display.repository')->getViewDisplay('node', 'page')
      ->setComponent($this->field_id)
      ->save();
    // Create and log in test user.
    $this->user = $this->drupalCreateUser([
      'administer node fields',
      'administer node form display',
      'administer node display',
      'create page content',
      'edit any page content',
    ]);
    $this->drupalLogin($this->user);
  }

  /**
   * Create test node.
   *
   * @param int $delta
   *
   * @return void
   */
  protected function createTestNode(int $delta) {
    $this->drupalGet('/node/add/page');
    for ($i = 0; $i < $delta; $i++) {
      $this->submitForm([], 'Add another item');
    }
    $edit = [
      'edit-title-0-value' => $this->randomMachineName(),
    ];
    for ($i = 0; $i <= $delta; $i++) {
      $edit["{$this->field_id}[$i][value]"] = $this->randomMachineName();
      $edit["{$this->field_id}[$i][_weight]"] = $i;

    }
    $this->submitForm($edit, 'Save');
  }
}
