<?php

declare(strict_types=1);

namespace Drupal\Tests\field_lock_multi_values\Functional;

use PHPUnit\Framework\Attributes\Group;

/**
 * Test "Remove table header" feature.
 */
#[Group('field_lock_multi_values')]
class RemoveTableHeaderTest extends TestBase {

  /**
   * Test "Remove table header" feature.
   */
  public function testRemoveTableHeader() : void {
    $page = $this->getSession()->getPage();
    $delta = 4;
    // Submit a test node.
    $this->createTestNode($delta);
    // Test node edit form with original functionality.
    $this->drupalGet('/node/1/edit');
    $this->assertSession()
      ->elementExists('css', '#field-string-textfield-values thead');
    // enable "Remove table header" feature.
    $this->drupalGet('/admin/structure/types/manage/page/form-display');
    $this->click('#edit-fields-field-string-textfield-settings-edit');
    $page->checkField('Remove table header');
    $this->submitForm([], 'Update');
    $this->submitForm([], 'Save');
    // Test node edit form with updated functionality.
    $this->drupalGet('/node/1/edit');
    $this->assertSession()
      ->elementNotExists('css', '#field-string-textfield-values thead');
  }
}
