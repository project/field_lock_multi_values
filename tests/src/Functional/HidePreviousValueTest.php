<?php

declare(strict_types=1);

namespace Drupal\Tests\field_lock_multi_values\Functional;

use PHPUnit\Framework\Attributes\Group;

/**
 * Test "Hide previous value" feature.
 */
#[Group('field_lock_multi_values')]
class HidePreviousValueTest extends TestBase {

  /**
   * Test "Hide previous value" feature.
   */
  public function testLockPreviousValue() : void {
    $page = $this->getSession()->getPage();
    $delta = 4;
    // Submit a test node.
    $this->createTestNode($delta);
    // Test node edit form with original functionality.
    $this->drupalGet('/node/1/edit');
    for ($i = 0; $i <= $delta; $i++) {
      $this->assertSession()
        ->fieldExists("edit-field-string-textfield-$i-value");
    }
    // enable "Hide previous value" feature.
    $this->drupalGet('/admin/structure/types/manage/page/form-display');
    $this->click('#edit-fields-field-string-textfield-settings-edit');
    $page->checkField('Hide previous value');
    $this->submitForm([], 'Update');
    $this->submitForm([], 'Save');
    // Test node edit form with updated functionality.
    $this->drupalGet('/node/1/edit');
    for ($i = 0; $i <= $delta; $i++) {
      $this->assertSession()
        ->fieldNotExists("edit-field-string-textfield-$i-value");
    }
    $this->assertSession()
      ->fieldExists("edit-field-string-textfield-$i-value");
  }
}
