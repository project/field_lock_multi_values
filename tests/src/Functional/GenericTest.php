<?php

namespace Drupal\Tests\field_lock_multi_values\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for field_lock_multi_values.
 *
 * @group field_lock_multi_values
 */
class GenericTest extends GenericModuleTestBase {}
