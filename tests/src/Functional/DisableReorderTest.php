<?php

declare(strict_types=1);

namespace Drupal\Tests\field_lock_multi_values\Functional;

use PHPUnit\Framework\Attributes\Group;

/**
 * Test "Disable reorder" feature.
 */
#[Group('field_lock_multi_values')]
class DisableReorderTest extends TestBase {

  /**
   * Test "Disable reorder" feature.
   */
  public function testDisableReorder() : void {
    $page = $this->getSession()->getPage();
    $delta = 4;
    // Submit a test node.
    $this->createTestNode($delta);
    // Test node edit form with original functionality.
    $this->drupalGet('/node/1/edit');
    for ($i = 0; $i <= $delta; $i++) {
      $this->assertSession()
        ->fieldExists("edit-field-string-textfield-$i-weight");
    }
    // enable "Disable reorder" feature.
    $this->drupalGet('/admin/structure/types/manage/page/form-display');
    $this->click('#edit-fields-field-string-textfield-settings-edit');
    $page->checkField('Disable reorder');
    $this->submitForm([], 'Update');
    $this->submitForm([], 'Save');
    // Test node edit form with updated functionality.
    $this->drupalGet('/node/1/edit');
    for ($i = 0; $i <= $delta; $i++) {
      $this->assertSession()
        ->fieldNotExists("edit-field-string-textfield-$i-weight");
    }
  }
}
