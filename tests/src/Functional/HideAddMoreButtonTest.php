<?php

declare(strict_types=1);

namespace Drupal\Tests\field_lock_multi_values\Functional;

use PHPUnit\Framework\Attributes\Group;

/**
 * Test "Hide 'Add more' button" feature.
 */
#[Group('field_lock_multi_values')]
class HideAddMoreButtonTest extends TestBase {

  /**
   * Test "Hide 'Add more' button" feature.
   */
  public function testHideAddMoreButton() : void {
    $page = $this->getSession()->getPage();
    $delta = 4;
    // Submit a test node.
    $this->createTestNode($delta);
    // Test node edit form with original functionality.
    $this->drupalGet('/node/1/edit');
    $this->assertSession()
      ->buttonExists('edit-field-string-textfield-add-more');
    // enable "Hide 'Add more' button" feature.
    $this->drupalGet('/admin/structure/types/manage/page/form-display');
    $this->click('#edit-fields-field-string-textfield-settings-edit');
    $page->checkField('Hide "Add more" button');
    $this->submitForm([], 'Update');
    $this->submitForm([], 'Save');
    // Test node edit form with updated functionality.
    $this->drupalGet('/node/1/edit');
    $this->assertSession()
      ->buttonNotExists('edit-field-string-textfield-add-more');

  }
}
